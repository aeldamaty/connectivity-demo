import asyncio
import json
import sqlite3
import sys
import xml.etree.ElementTree as ET

from aiohttp import ClientSession
from sqlite3 import Error


async def post_json(json, session):
	url = "https://experimentation.getsnaptravel.com/interview/hotels"
	async with session.post(url, json=json) as response:
		return await response.json()

async def post_xml(data, session):
	url = "https://experimentation.getsnaptravel.com/interview/legacy_hotels"
	headers = {'Content-Type': 'application/xml'}
	async with session.post(url, data=data, headers=headers) as response:
		return await response.text()

async def make_requests(json_data, xml_data):
	tasks = []

	async with ClientSession() as session:
		json_task = asyncio.ensure_future(post_json(json_data, session))
		xml_task = asyncio.ensure_future(post_xml(xml_data, session))
		tasks = [json_task, xml_task]
		responses = await asyncio.gather(*tasks)
	return responses

def parse_json_responses(response):
	hotel_dictionary = {}
	for hotel in response['hotels']:
		hotel_dictionary[hotel['id']] = hotel
	return hotel_dictionary

def parse_xml_responses(response):
	root = ET.fromstring(response)
	legacy_hotel_dictionary = {}
	for element in root:
		hotel_id = int(element.find("id").text)
		price = element.find("price").text
		legacy_hotel_dictionary[hotel_id] = price

	return legacy_hotel_dictionary

def create_connection(db_file):
	conn = None
	try:
		conn = sqlite3.connect(db_file)
		return conn
	except Error as e:
		print(e)
	return conn

def create_table(conn, sql):
	try:
		c = conn.cursor()
		c.execute(sql)
	except Error as e:
		print(e)

def create_hotel(conn, hotel):
	sql = '''INSERT OR REPLACE INTO hotels(id, hotel_name, num_reviews, address, num_stars, amenities, image_url, prices)
		VALUES(?,?,?,?,?,?,?,?) '''
	cur = conn.cursor()
	cur.execute(sql, hotel)
	return cur.lastrowid

def save_to_db(hotels, legacy_hotels, hotels_to_use):
	database = r"C:\Users\aser_\code\sqlite\db\pythonsqlite.db"
	sql_create_table = """CREATE TABLE IF NOT EXISTS hotels (
		id integer PRIMARY KEY,
		hotel_name text,
		num_reviews integer,
		address text,
		num_stars integer,
		amenities blob,
		image_url text,
		prices blob
	); """

	conn = create_connection(database)

	if conn is not None:
		create_table(conn, sql_create_table)

		for hotel_id in hotels_to_use:
			hotel_to_create = (
				hotel_id,
				hotels[hotel_id]["hotel_name"],
				hotels[hotel_id]["num_reviews"],
				hotels[hotel_id]["address"],
				hotels[hotel_id]["num_stars"],
				json.dumps(hotels[hotel_id]["amenities"]),
				hotels[hotel_id]["image_url"],
				json.dumps(
					{
						"snaptravel": hotels[hotel_id]['price'],
						"retail": legacy_hotels[hotel_id]
					}
				)
			)
			with conn:
				hotel_id = create_hotel(conn, hotel_to_create)


def main(argv):
	print("Welcome to the connectivity demo")
	city = input("Please enter your city: ")
	checkin = input("Please enter checkin date: ")
	checkout = input("Please enter checkout date: ")

	json_request_data = {
		"city": city,
		"checkin": checkin,
		"checkout": checkout,
		"provider": "snaptravel"
	}

	xml_request_data = """
		<?xml version="1.0" encoding="UTF-8"?>
		<root>
			<checkin>{0}</checkin>
			<checkout>{1}</checkout>
			<city>{2}</city>
			<provider>snaptravel</provider>
		</root>
	""".format(checkin, checkout, city)

	loop = asyncio.get_event_loop()
	future = asyncio.ensure_future(make_requests(json_request_data, xml_request_data))
	responses = loop.run_until_complete(future)

	hotel_dictionary = parse_json_responses(responses[0])
	legacy_hotel_dictionary = parse_xml_responses(responses[1])

	set_hotel = set(hotel_dictionary.keys())
	set_legacy = set(legacy_hotel_dictionary.keys())
	hotels_to_use = set_hotel & set_legacy

	save_to_db(hotel_dictionary, legacy_hotel_dictionary, hotels_to_use)

	print("Successfully saved to the database")

if __name__ == "__main__":
	main(sys.argv[1:])